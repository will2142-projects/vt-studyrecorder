#ifndef _BEEPHELPER_H_
#define _BEEPHELPER_H_

#include <windows.h>

void startBeeps()
{
		Beep(500, 500);
		Beep(1000, 500);
}

void stopBeeps()
{
		Beep(1000, 500);
		Beep(500, 500);
}

void errorBeeps()
{
		// Closing Beeps
		Beep(1500, 500);
		Beep(1500, 500);
		Beep(1500, 500);
		Beep(1500, 500);
		Beep(1500, 500);
		Beep(1500, 500);
}

#endif