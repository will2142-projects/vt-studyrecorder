#ifndef _HELPERATI_H_
#define _HELPERATI_H_

#include ".\atidaq\ftconfig.h"
#include <string>

Calibration *cal;			// struct containing calibration information

int initializeATI(void) {
	char* calfilepath = "FT8859.cal";			// name of calibration file
	unsigned short index = 1;								// index of calibration in file (second parameter; default = 1)
	int sts = 0;	// temporary variable to retrieve function returns.

	// create Calibration struct
	cal=createCalibration(calfilepath,index);
	if (cal==NULL) {
		printf("\nSpecified calibration could not be loaded.\n");
		scanf(".");
		return 0;
	}

	// Set force units.
	// This step is optional; by default, the units are inherited from the calibration file.
	sts=SetForceUnits(cal,"N");
	switch (sts) {
		case 0: break;	// successful completion
		case 1: printf("Invalid Calibration struct"); return 0;
		case 2: printf("Invalid force units"); return 0;
		default: printf("Unknown error"); return 0;
	}

	// Set torque units.
	// This step is optional; by default, the units are inherited from the calibration file.
	sts=SetTorqueUnits(cal,"N-m");
	switch (sts) {
		case 0: break;	// successful completion
		case 1: printf("Invalid Calibration struct"); return 0;
		case 2: printf("Invalid torque units"); return 0;
		default: printf("Unknown error"); return 0;
	}

	// Set tool transform.
	// This line is only required if you want to move or rotate the sensor's coordinate system.
	// This example tool transform translates the coordinate system 20 mm along the Z-axis 
	// and rotates it 45 degrees about the X-axis.
	// This sample transform includes a translation along the Z-axis and a rotation about the X-axis.
	/*
	float SampleTT[6]={0,0,20,45,0,0};
	sts=SetToolTransform(cal,SampleTT,"mm","degrees");
	switch (sts) {
		case 0: break;	// successful completion
		case 1: printf("Invalid Calibration struct"); return 0;
		case 2: printf("Invalid distance units"); return 0;
		case 3: printf("Invalid angle units"); return 0;
		default: printf("Unknown error"); return 0;
	}
	*/	

	// Temperature compensation is on by default if it is available.
	// To explicitly disable temperature compensation, uncomment the following code
	/*SetTempComp(cal,FALSE);                   // disable temperature compensation
	switch (sts) {
		case 0: break;	// successful completion
		case 1: printf("Invalid Calibration struct"); return 0;
		case 2: printf("Temperature Compensation not available on this transducer"); return 0;
		default: printf("Unknown error"); return 0;
	}*/

	return 1; // no errors
}

void getNewBiasATI(float voltages[]) {	
	// store an unloaded measurement; this removes the effect of tooling weight
	Bias(cal,voltages);
}

void exitHandleATI() {
	// free memory allocated to Calibration structure
	destroyCalibration(cal);
}

void convertVoltsToForceTorques(float voltages[], float ForceTorques[]) {
			 
	//convert a loaded measurement into forces and torques
	
	ConvertToFT(cal,voltages,ForceTorques);
}
#endif