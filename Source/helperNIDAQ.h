#ifndef _HELPERNIDAQ_H_
#define _HELPERNIDAQ_H_

#include "NIDAQmx.h"
#include "helperATI.h"
#include "performanceCounter.h"
#include "beepHelper.h"

// User set parameters
#define MIN_VOLT_FT			-10
#define MAX_VOLT_FT			 10

#define MIN_VOLT_ACC		 -5
#define MAX_VOLT_ACC		  5

#define MIN_VOLT_CURRENT	 -5
#define MAX_VOLT_CURRENT	  5

#define SAMPLE_RATE				5000
#define SAMPLES_PER_CHANNEL		5000
#define NUM_CHANNELS			9

// Error check macros
#define gotoDAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else
#define gotoDAQmxErrChk2(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error2; else
#define returnDAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) return error; else

//Global timer
PerformanceCounter timer;

// Callback Function Prototypes
//int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);
//int32 CVICALLBACK DoneCallback(TaskHandle taskHandle, int32 status, void *callbackData);

int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData)
{
	int32       error=0;
	char        errBuff[2048]={'\0'};
	static int  totalNumRead=0;
	static int  totalNumWrote=0;
	static float voltages[6];
	static float forcesAndTorques32[6];
	static float accelerations[2];
	static float syncVoltage[1];
	//static float64 forcesAndTorques64[6];

	int32       numSamplesReadPerChannel=0;
	float64     data[NUM_CHANNELS * SAMPLES_PER_CHANNEL];
	//std::vector<float64> data(NUM_CHANNELS * SAMPLES_PER_CHANNEL);
		
	/*********************************************/
	// DAQmx Read Code
	/*********************************************/
	//printf("Started Callback.\n");
	timer.StopCounter();
	double time = timer.GetElapsedTime();

	gotoDAQmxErrChk (DAQmxReadAnalogF64(taskHandle, SAMPLES_PER_CHANNEL, 0.0, DAQmx_Val_GroupByScanNumber, data, NUM_CHANNELS * SAMPLES_PER_CHANNEL, &numSamplesReadPerChannel,NULL));
	if( numSamplesReadPerChannel>0 ) {
		printf("Recorded for approximately %.2f seconds. Total samples acquired: %d\r", time, totalNumRead+=numSamplesReadPerChannel);
		fflush(stdout);

		if(callbackData != NULL) {
			for (int i = 0; i < SAMPLES_PER_CHANNEL*NUM_CHANNELS; i+=NUM_CHANNELS)
			{
				voltages[0] = (float)data[i];
				voltages[1] = (float)data[i+1];
				voltages[2] = (float)data[i+2];
				voltages[3] = (float)data[i+3];
				voltages[4] = (float)data[i+4];
				voltages[5] = (float)data[i+5];
				accelerations[0] = (float)data[i+6];
				accelerations[1] = (float)data[i+7];
				syncVoltage[0] = (float)data[i+8];
				
				convertVoltsToForceTorques(voltages, forcesAndTorques32);

				fwrite(forcesAndTorques32, sizeof(float), 6, (FILE*)callbackData);
				fwrite(accelerations,      sizeof(float), 2, (FILE*)callbackData);
				fwrite(syncVoltage,        sizeof(float), 1, (FILE*)callbackData);
			}
		}
		
		//fwrite(data, sizeof(float64), NUM_CHANNELS * SAMPLES_PER_CHANNEL, (FILE*)callbackData);

		
/*      //Code if you wanted to write to file as delimited text.
		for(int i=0; i<(NUM_CHANNELS*SAMPLES_PER_CHANNEL); i++) {
			fprintf((FILE*) callbackData, "%f", data[i]);
			if ( i % 8 == 7)
				fprintf((FILE*) callbackData, "\n");
			else
				fprintf((FILE*) callbackData, "\t");
		}
*/
		//printf("Data written to File...\n");
		//fflush(stdout);
	}

Error:
	if( DAQmxFailed(error) ) {
		DAQmxGetExtendedErrorInfo(errBuff,2048);
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		DAQmxStopTask(taskHandle);
		DAQmxClearTask(taskHandle);
		printf("DAQmx Error: %s\n",errBuff);
		errorBeeps();
		return -1;
	}
	return 0;
}

int32 CVICALLBACK DoneCallback(TaskHandle taskHandle, int32 status, void *callbackData)
{
	int32   error=0;
	char    errBuff[2048]={'\0'};

	// Check to see if an error stopped the task.
	gotoDAQmxErrChk (status);
	
Error:
	if( DAQmxFailed(error) ) {
		DAQmxGetExtendedErrorInfo(errBuff,2048);		
		printf("DAQmx Error: %s\n",errBuff);
		DAQmxClearTask(taskHandle);
	}
	return 0;
}

int32 startContinuousAnalogRecording(FILE* dataFile, TaskHandle& inputTaskHandle, TaskHandle& outputTaskHandle) {
	// Variables for NI Error Handling
	int32       error=0;
		
	/*********************************************/
	// DAQmx Configure Code
	/*********************************************/
	returnDAQmxErrChk (DAQmxCreateTask("GetAnalog",&inputTaskHandle));

	/*** Set up ATI FT sensor  ***/
	//(NOTE: these are straight voltage values, must be processed using the appropriate calibration file.)
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(inputTaskHandle,"Dev1/ai0","FT-0",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(inputTaskHandle,"Dev1/ai1","FT-1",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(inputTaskHandle,"Dev1/ai2","FT-2",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(inputTaskHandle,"Dev1/ai3","FT-3",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(inputTaskHandle,"Dev1/ai4","FT-4",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(inputTaskHandle,"Dev1/ai5","FT-5",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
	
	// Set up the accelerometers
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(inputTaskHandle,"Dev1/ai16","SlaveAx",DAQmx_Val_RSE,MIN_VOLT_ACC,MAX_VOLT_ACC,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(inputTaskHandle,"Dev1/ai17","SlaveBx",DAQmx_Val_RSE,MIN_VOLT_ACC,MAX_VOLT_ACC,DAQmx_Val_Volts,NULL));
	
	// Synchronizer 
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(inputTaskHandle,"Dev1/ai21","Synch",DAQmx_Val_RSE,0.0,5.0,DAQmx_Val_Volts,NULL));

	// Set up timing.
	returnDAQmxErrChk (DAQmxCfgSampClkTiming(inputTaskHandle, "OnboardClock", SAMPLE_RATE, DAQmx_Val_Rising, DAQmx_Val_ContSamps, SAMPLES_PER_CHANNEL));
	returnDAQmxErrChk (DAQmxRegisterEveryNSamplesEvent(inputTaskHandle, DAQmx_Val_Acquired_Into_Buffer, SAMPLES_PER_CHANNEL, 0, EveryNCallback, dataFile));
	
	// Registers a callback function to receive an event when a task stops due to an error or when a finite acquisition task or finite generation 
	// task completes execution. A Done event does not occur when a task is stopped explicitly, such as by calling DAQmxStopTask.
	returnDAQmxErrChk (DAQmxRegisterDoneEvent(inputTaskHandle,0,DoneCallback,NULL));

/*------------------------------------------------------*/
	const int32 outputSamplesLength = 25;
	const float64 outputSampleRate = 5.0;	// Hz	
	float64 outputData[outputSamplesLength];
	const float64 ON  = 2.0;
	const float64 OFF   = 0.0;
	int32   	written;

	outputData[0] = ON;
	outputData[1] = ON;
	outputData[2] = ON;
	outputData[3] = ON;
	outputData[4] = ON;
	outputData[5] = OFF;
	outputData[6] = OFF;
	outputData[7] = ON;
	outputData[8] = ON;
	outputData[9] = ON;
	outputData[10] = OFF;
	outputData[11] = OFF;
	outputData[12] = OFF;
	outputData[13] = ON;
	outputData[14] = ON;
	outputData[15] = OFF;
	outputData[16] = OFF;
	outputData[17] = ON;
	outputData[18] = OFF;
	outputData[19] = OFF;
	outputData[20] = OFF;
	outputData[21] = ON;
	outputData[22] = ON;
	outputData[23] = ON;
	outputData[24] = OFF;

	
	/*********************************************/
	// DAQmx Configure Code
	/*********************************************/
	returnDAQmxErrChk (DAQmxCreateTask("SynchPulsesSTART",&outputTaskHandle));
	returnDAQmxErrChk (DAQmxCreateAOVoltageChan(outputTaskHandle,"Dev1/ao3","",0.0,5.0,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCfgSampClkTiming(outputTaskHandle,"",outputSampleRate,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,outputSamplesLength));
	

	/*********************************************/
	// DAQmx Write Code
	/*********************************************/
	returnDAQmxErrChk (DAQmxWriteAnalogF64(outputTaskHandle,outputSamplesLength,0,10.0,DAQmx_Val_GroupByChannel,outputData,&written,NULL));

	printf("\n\nPlease place LED synchronizer in the camera view and then press 'r' to begin recording.\n");
	while(1) {
		if(_kbhit()) {	// Check the keypress buffer
			char keycheck = getch();
			if(keycheck == 'r') // r key to continue and start recording
				break;
			else
				printf("-->You Pressed: ASCII %d. Please press 'r' key to begin recording.\n", keycheck);
		}
	}

	/*********************************************/
	// DAQmx Start Code
	/*********************************************/
	returnDAQmxErrChk (DAQmxStartTask(outputTaskHandle));

/*------------------------------------------------------*/
	// DAQmx Start Task
	returnDAQmxErrChk (DAQmxStartTask(inputTaskHandle));
	
	timer.StartCounter();

	// Starting Recording Beeps
	startBeeps();

	/*********************************************/
	// DAQmx Wait Code
	/*********************************************/
	//printf("Allow synchronization flashes to complete.\n");
	//returnDAQmxErrChk (DAQmxWaitUntilTaskDone(outputTaskHandle,10.0));
	//DAQmxStopTask(outputTaskHandle);
	
	// Delay stopping the Task until the 'q' key has been pressed.
	printf("\nSTARTED Recording samples continuously. Press 'q' to stop the recording.\n");
	while(1) {
		if(_kbhit()) {	// Check the keypress buffer
			char keycheck = getch();
			if(keycheck == 'q') // q key to quit
				break;
			//if(keycheck == 'e') // The following is an unsupported property for the NI USB-6259, so it flags causes an error.  For debugging purposes.
			//	returnDAQmxErrChk ( DAQmxSetAOIdleOutputBehavior(outputTaskHandle, "Dev1/a03", DAQmx_Val_ZeroVolts ));
			else
				printf("-->You Pressed: ASCII %d. Please press 'q' key to quit.\n", keycheck);
		}
	}
	printf("\n*** Detected 'q' key press, quiting... \n");
	
	// Upon keypress wait enough time for the current sampling task to finish.
	DAQmxStopTask(outputTaskHandle);
	returnDAQmxErrChk (DAQmxStartTask(outputTaskHandle));

	// Stopping Beeps
	stopBeeps();

	returnDAQmxErrChk (DAQmxWaitUntilTaskDone(outputTaskHandle,10.0));
	
	//int waitTime = (int)(1000.0/SAMPLE_RATE * SAMPLES_PER_CHANNEL);
	//printf("Detected ENTER Press. Waiting for the end of the current acquisition... %d\n", waitTime);
	//Sleep(waitTime);

	timer.StopCounter();
	double time = timer.GetElapsedTime();

	returnDAQmxErrChk (DAQmxStopTask(inputTaskHandle));
	returnDAQmxErrChk (DAQmxClearTask(inputTaskHandle));

	returnDAQmxErrChk (DAQmxStopTask(outputTaskHandle));
	returnDAQmxErrChk (DAQmxClearTask(outputTaskHandle));

	printf("\n***Acquired data for approximately %1.3f seconds.\n", time);
	//printf("Expect approximately  %1.f samples.\n", time * SAMPLE_RATE);

	return 0;
}

int32 CVICALLBACK BiasStartCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData)
{
	int32       error=0;
	char        errBuff[2048]={'\0'};
	static int  totalNumRead=0;
	static int  totalNumWrote=0;
	float voltages[6];

	voltages[0] = 0;
	voltages[1] = 0;
	voltages[2] = 0;
	voltages[3] = 0;
	voltages[4] = 0;
	voltages[5] = 0;

	int32       numSamplesReadPerChannel=0;
	float64     data[6 * SAMPLES_PER_CHANNEL];
		
	/*********************************************/
	// DAQmx Read Code
	/*********************************************/
		
	gotoDAQmxErrChk (DAQmxReadAnalogF64(taskHandle, SAMPLES_PER_CHANNEL, 0.0, DAQmx_Val_GroupByScanNumber, data, 6 * SAMPLES_PER_CHANNEL, &numSamplesReadPerChannel,NULL));
	if( numSamplesReadPerChannel>0 ) {	
		for (int i = 0; i < SAMPLES_PER_CHANNEL*6; i+=6) {
				voltages[0] += (float)data[i];
				voltages[1] += (float)data[i+1];
				voltages[2] += (float)data[i+2];
				voltages[3] += (float)data[i+3];
				voltages[4] += (float)data[i+4];
				voltages[5] += (float)data[i+5];				
		}
	}

	voltages[0] = voltages[0]/SAMPLES_PER_CHANNEL;
	voltages[1] = voltages[1]/SAMPLES_PER_CHANNEL;
	voltages[2] = voltages[2]/SAMPLES_PER_CHANNEL;
	voltages[3] = voltages[3]/SAMPLES_PER_CHANNEL;
	voltages[4] = voltages[4]/SAMPLES_PER_CHANNEL;
	voltages[5] = voltages[5]/SAMPLES_PER_CHANNEL;
	
	getNewBiasATI(voltages);
	printf("Finished Biasing...\n");
Error:
	if( DAQmxFailed(error) ) {
		DAQmxGetExtendedErrorInfo(errBuff,2048);
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		DAQmxStopTask(taskHandle);
		DAQmxClearTask(taskHandle);
		printf("DAQmx Error: %s\n",errBuff);
	}
	return 0;
}

int32 gatherDataToZeroATI(TaskHandle& taskHandle) {
	
		// Variables for NI Error Handling
	int32       error=0;
	
	/*********************************************/
	// DAQmx Configure Code
	/*********************************************/
	returnDAQmxErrChk (DAQmxCreateTask("ZeroATI",&taskHandle));

	/*** Set up ATI FT sensor  ***/
	//(NOTE: these are straight voltage values, must be processed using the appropriate calibration file.)
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandle,"Dev1/ai0","FT-0",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandle,"Dev1/ai1","FT-1",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandle,"Dev1/ai2","FT-2",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandle,"Dev1/ai3","FT-3",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandle,"Dev1/ai4","FT-4",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
	returnDAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandle,"Dev1/ai5","FT-5",DAQmx_Val_Diff,MIN_VOLT_FT,MAX_VOLT_FT,DAQmx_Val_Volts,NULL));
			
	// Set up timing.
	returnDAQmxErrChk (DAQmxCfgSampClkTiming(taskHandle, "OnboardClock", SAMPLE_RATE, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, SAMPLES_PER_CHANNEL));

	returnDAQmxErrChk (DAQmxRegisterEveryNSamplesEvent(taskHandle, DAQmx_Val_Acquired_Into_Buffer, SAMPLES_PER_CHANNEL, 0, BiasStartCallback, NULL));
	
	// Registers a callback function to receive an event when a task stops due to an error or when a finite acquisition task or finite generation 
	// task completes execution. A Done event does not occur when a task is stopped explicitly, such as by calling DAQmxStopTask.
	returnDAQmxErrChk (DAQmxRegisterDoneEvent(taskHandle,0,DoneCallback,NULL));

	// DAQmx Start Task
	returnDAQmxErrChk (DAQmxStartTask(taskHandle));
	printf("Waiting to gather data and bias...");
	returnDAQmxErrChk (DAQmxWaitUntilTaskDone(taskHandle,10.0));

	DAQmxStopTask(taskHandle);
	DAQmxClearTask(taskHandle);

	return 0;
}

#endif
