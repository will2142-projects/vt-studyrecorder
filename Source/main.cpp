/*********************************************************************
* Author: William McMahan
* Date: 5/16/2010
* DataLogger program is based upon starter code NI-DAQ "ContAcq-IntClk.c"
*
* Notes from the starter code.
**********************************************************************
* ANSI C Example program:
*    ContAcq-IntClk.c
*
* Example Category:
*    AI
*
* Description:
*    This example demonstrates how to acquire a continuous amount of
*    data using the DAQ device's internal clock.
*
* Instructions for Running:
*    1. Select the physical channel to correspond to where your
*       signal is input on the DAQ device.
*    2. Enter the minimum and maximum voltage range.
*    Note: For better accuracy try to match the input range to the
*          expected voltage level of the measured signal.
*    3. Set the rate of the acquisition. Also set the Samples per
*       Channel control. This will determine how many samples are
*       read at a time. This also determines how many points are
*       plotted on the graph each time.
*    Note: The rate should be at least twice as fast as the maximum
*          frequency component of the signal being acquired.
*
* Steps:
*    1. Create a task.
*    2. Create an analog input voltage channel.
*    3. Set the rate for the sample clock. Additionally, define the
*       sample mode to be continuous.
*    4. Call the Start function to start the acquistion.
*    5. Read the data in the EveryNCallback function until the stop
*       button is pressed or an error occurs.
*    6. Call the Clear Task function to clear the task.
*    7. Display an error if any.
*
* I/O Connections Overview:
*    Make sure your signal input terminal matches the Physical
*    Channel I/O control. For further connection information, refer
*    to your hardware reference manual.
*
*********************************************************************/

#include <stdio.h>
#include <conio.h>		// Needed for _kbhit()
#include <windows.h>
#include <time.h>
#include <string>
#include <iostream>	
#include "helperNIDAQ.h"
#include "fileHelper.h"
#include "beepHelper.h"


int main(void)
{
	printf("---------------------------------------------------\n");
	printf("NI-DAQ Data Logger - Verrotouch Edition\n");
	printf("(WCM 6/3/2010)\n");
	printf("---------------------------------------------------\n");

	// Initialize ATI
	if( !initializeATI() ){
		printf("ERROR: Failed to Initialize ATI.\n");
		getchar();
		return 1;
	}

	// Setup the file variables
	std::string subjectNumber;
	std::string feedbackCondition;
	std::string taskName;
	std::string filename = generateFilename(subjectNumber, feedbackCondition, taskName);

	if(fileExists(filename)) {
		
		printf("File %s already exists!\n", filename.c_str());
		
		if( !permissionToOverwrite(filename) ) {
			printf("Restart program to select a new file name...\n");
			getchar();
			getchar();
			return -1;
		}

	}
	else {
		if(!isTheFilenameCorrect(filename)) {
			printf("Restart program to select a new file name...\n");
			getchar();
			getchar();
			return -1;
		}

	}

	// Open the file for writing.
	FILE* dataFile;
	dataFile = fopen( filename.c_str(), "wb");
	int writeStatus = 0;
		
	/* Write a header for the file. */
	writeStatus = fprintf(dataFile, "FILE_NAME:\t%s\n",filename.c_str() );
	writeStatus = fprintf(dataFile, "SUBJECT_NUMBER:\t%s\n",subjectNumber.c_str() );
	writeStatus = fprintf(dataFile, "CONDITION:\t%s\n", feedbackCondition.c_str() );
	writeStatus = fprintf(dataFile, "TASK:\t%s\n", taskName.c_str() );

	// Write a time stamp.
	time_t ltime; /* calendar time */
    ltime=time(NULL); /* get current cal time */
	writeStatus = fprintf(dataFile, "TIME_STAMP:\t%s",asctime( localtime(&ltime) ) );
	
	writeStatus = fprintf(dataFile, "FT_SENSOR:\tATI Mini 40-2, FT8859\n");
	writeStatus = fprintf(dataFile, "FORCE_UNIT:\tN\n");
	writeStatus = fprintf(dataFile, "TORQUE_UNIT:\tN-m\n");
	writeStatus = fprintf(dataFile, "ACC_SENSOR:\tADXL322, 5V, ?Hz\n");
	writeStatus = fprintf(dataFile, "ACC_UNIT:\tV\n");
	writeStatus = fprintf(dataFile, "SAMPLE_RATE:\t%i\n", SAMPLE_RATE);
	writeStatus = fprintf(dataFile, "DATA_FORMAT:\tFLOAT\n");
	writeStatus = fprintf(dataFile, "DATA: [Fx, Fy, Fz, Tx, Ty, Tz, acc1, acc2, sync]\n");
	if (writeStatus < 0) {
		printf("** Error writing header data to file. Is the file open? ***\n");
		goto Error;
	}

	//float dummyData[9] = {5.5, 5.5, 5.5, 5.5, 5.5, 5.5, 5.5, 5.5, 5.5};
	//for(int i = 0; i <100; i++)
	//	fwrite(dummyData, sizeof(float), 9, dataFile);	

	// Variables for NI Error Handling
	int32 error=0;
	
	// TaskHandle for analog input
	TaskHandle  taskHandleForAnalogGather = 0;
	TaskHandle  taskHandleForZeroATI = 0;
	TaskHandle  taskHandleForOutput = 0;

	/*********************************************/
	// DAQmx Write Code
	/*********************************************/

	// Zero the ATI Mini-40
	//gotoDAQmxErrChk( gatherDataToZeroATI(taskHandleForZeroATI) );
	
	// Start continuous
	gotoDAQmxErrChk (startContinuousAnalogRecording(dataFile, taskHandleForAnalogGather, taskHandleForOutput));

Error:
	char errBuff[2048]={'\0'};
	if( DAQmxFailed(error) ) {
		DAQmxGetExtendedErrorInfo(errBuff,2048);
		errorBeeps();

	}
	if( taskHandleForZeroATI!=0 ) {
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		DAQmxStopTask(taskHandleForZeroATI);
		DAQmxClearTask(taskHandleForZeroATI);
	}
	if( taskHandleForAnalogGather!=0 ) {
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		DAQmxStopTask(taskHandleForAnalogGather);
		DAQmxClearTask(taskHandleForAnalogGather);
	}
	if( taskHandleForOutput!=0 ) {
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		DAQmxStopTask(taskHandleForOutput);
		DAQmxClearTask(taskHandleForOutput);
	}
	if( DAQmxFailed(error) )
		printf("DAQmx Error: %s\n",errBuff);

	// Try to Zero out the analog output
	TaskHandle  taskHandleForClosingAnalogOut = 0;

	/*********************************************/
	// DAQmx Configure Code
	/*********************************************/
	printf("\n***Zeroing the Analog Output Channel.\n");
	gotoDAQmxErrChk2 (DAQmxCreateTask("ZeroAO_at_End",&taskHandleForClosingAnalogOut));
	gotoDAQmxErrChk2 (DAQmxCreateAOVoltageChan(taskHandleForClosingAnalogOut,"Dev1/ao3","",0.0,5.0,DAQmx_Val_Volts,NULL));
	gotoDAQmxErrChk2 (DAQmxCfgSampClkTiming(taskHandleForClosingAnalogOut,"",1000,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,5));

	float64 closeData[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
	int32 tmp = 0;
	gotoDAQmxErrChk2 ( DAQmxWriteAnalogF64(taskHandleForClosingAnalogOut,5,0,10.0,DAQmx_Val_GroupByChannel,closeData,&tmp,NULL) );
	
	gotoDAQmxErrChk2 (DAQmxStartTask(taskHandleForClosingAnalogOut));
	gotoDAQmxErrChk2 (DAQmxWaitUntilTaskDone(taskHandleForClosingAnalogOut,2.0));
	
Error2:
	if( DAQmxFailed(error) )
		printf("DAQmx Error: %s\n",errBuff);
	DAQmxStopTask(taskHandleForClosingAnalogOut);
	DAQmxClearTask(taskHandleForClosingAnalogOut);

	// Close out the ATI Device
	exitHandleATI();

	// Close out the file.
	fclose(dataFile);
	printf("End of program, press Enter key to quit\n");
	getchar();
	return 0;
}