%Conversion Factor for accelerometer 
%NOTE: This is for the ADXL322, 5V accelerometer
ACC_VOLTS_TO_MS2 = 1/(.75 * 9.81)   % m/s^2 / V

fid = fopen('25_V_hello.dat', 'r');

% Read the header... this will need to be parsed later.
h_filename = fgetl(fid)
h_subject = fgetl(fid)
h_condition = fgetl(fid)
h_task = fgetl(fid)
h_timestamp = fgetl(fid)
h_ft_sensor = fgetl(fid)
h_force_unit = fgetl(fid)
h_torque_unit = fgetl(fid)
h_acc_sensor = fgetl(fid)
h_acc_unit = fgetl(fid)
h_sample_rate = fgetl(fid)
h_data_format = fgetl(fid)
h_data_streams = fgetl(fid)

[junk, sample_rate] = strtok(h_sample_rate);
sample_rate = str2num(sample_rate);

[junk, data_format] = strtok(h_data_format);
data_format = lower(strtrim(data_format));

% The number, order and name of these data streams can possibly be pulled from the
% header info, but is hard coded for now.
num_channels = 9;
data = fread(fid, [num_channels,inf], data_format);
fclose(fid);

% Create a time vector.
t = [0:length(data)-1]/sample_rate;

Fx = data(1,:);
Fy = data(2,:);
Fz = data(3,:);
Tx = data(4,:);
Ty = data(5,:);
Tz = data(6,:);

% should specify left and right or A and B
acc1_V = data(7,:) - mean(data(7,:)); % subtract the gravity vector
acc2_V = data(8,:) - mean(data(8,:));
acc1 = acc1_V * ACC_VOLTS_TO_MS2;
acc2 = acc2_V * ACC_VOLTS_TO_MS2;

sync = data(9,:);

% plot the data.
figure(1); clf;
plot(t, Fx, 'r', t, Fy, 'g', t, Fz, 'b');
xlabel('Time (s)')
ylabel('Forces (N)')

figure(2); clf;
plot(t, Tx, 'r', t, Ty, 'g', t, Tz, 'b');
xlabel('Time (s)')
ylabel('Torques (N-m)')

figure(3); clf;
plot(t, acc1, 'r', t, acc2, 'g');
xlabel('Time (s)')
ylabel('Accelerations (m/s^2)')

figure(4); clf;
plot(t, sync, 'r');
xlabel('Time (s)')
ylabel('Synch Pulses (V)')